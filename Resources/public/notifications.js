$(document).ready(function() {
    var urlUnread = Routing.generate('notification_unread_number');
    var request = function() {
        $.ajax(urlUnread).done(function(msg) {
            console.log(msg);
            if(parseInt(msg['count']['nb'])) {
                $('.notifications-badge').text(msg['count']['nb']);
                $('.notifications-badge').removeClass('hidden');
            } else {
                $('.notifications-badge').text('0');
                $('.notifications-badge').addClass('hidden');
            }
        });
    };
    request();
    setInterval(request, 15000);
});