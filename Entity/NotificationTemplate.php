<?php

namespace Trendix\NotificationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NotificationTemplate
 *
 * @ORM\Table(name="notification_template")
 * @ORM\Entity(repositoryClass="Trendix\NotificationsBundle\Repository\NotificationTemplateRepository")
 */
class NotificationTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="eventType", type="string", length=255, unique=true)
     */
    private $eventType;

    /**
     * @var string
     *
     * @ORM\Column(name="emailSubject", type="string", length=255, nullable=true)
     */
    private $emailSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="emailBody", type="string", length=255, nullable=true)
     */
    private $emailBody;

    /**
     * @var string
     *
     * @ORM\Column(name="notificationSubject", type="string", length=255)
     */
    private $notificationSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="notificationBody", type="string", length=255)
     */
    private $notificationBody;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventType
     *
     * @param string $eventType
     * @return NotificationTemplate
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return string 
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Set emailSubject
     *
     * @param string $emailSubject
     * @return NotificationTemplate
     */
    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;

        return $this;
    }

    /**
     * Get emailSubject
     *
     * @return string 
     */
    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    /**
     * Set emailBody
     *
     * @param string $emailBody
     * @return NotificationTemplate
     */
    public function setEmailBody($emailBody)
    {
        $this->emailBody = $emailBody;

        return $this;
    }

    /**
     * Get emailBody
     *
     * @return string 
     */
    public function getEmailBody()
    {
        return $this->emailBody;
    }

    /**
     * Set notificationSubject
     *
     * @param string $notificationSubject
     * @return NotificationTemplate
     */
    public function setNotificationSubject($notificationSubject)
    {
        $this->notificationSubject = $notificationSubject;

        return $this;
    }

    /**
     * Get notificationSubject
     *
     * @return string 
     */
    public function getNotificationSubject()
    {
        return $this->notificationSubject;
    }

    /**
     * Set notificationBody
     *
     * @param string $notificationBody
     * @return NotificationTemplate
     */
    public function setNotificationBody($notificationBody)
    {
        $this->notificationBody = $notificationBody;

        return $this;
    }

    /**
     * Get notificationBody
     *
     * @return string 
     */
    public function getNotificationBody()
    {
        return $this->notificationBody;
    }
}
