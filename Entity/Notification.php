<?php

namespace Trendix\NotificationsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Trendix\AdminBundle\Entity\User;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="Trendix\NotificationsBundle\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="readBy", type="simple_array", nullable=true)
     */
    private $readBy;

    /**
     * @var array
     *
     * @ORM\Column(name="receivers", type="simple_array")
     */
    private $receivers;

    /**
     * @ORM\ManyToOne(targetEntity="NotificationTemplate")
     * @ORM\JoinColumn(name="template", referencedColumnName="id")
     */
    private $template;

    /**
     * @var array
     *
     * @ORM\Column(name="data", type="array")
     */
    private $data;

    /**
     * Notification constructor.
     * @param string $link Link the notification and email will refer to when user click on them
     * @param $template NotificationTemplate to use
     * @param $users array who has to receive the notification
     * @param $data array Data use to print the notifications
     */
    public function __construct($link, $template, $users, $data)
    {
        $this->createdAt = new \DateTime('now');
        $this->link = $link;
        $this->template = $template;
        $this->receivers = $users;
        $this->data = $data;
        $this->readBy = array();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return Notification
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Notification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set readBy
     *
     * @param array $readBy
     * @return Notification
     */
    public function setReadBy($readBy)
    {
        $this->readBy = $readBy;

        return $this;
    }

    /**
     * Add read by user
     *
     * @param integer $readBy
     * @return Notification
     */
    public function addReadBy($readBy)
    {
        if(array_search($readBy, $this->readBy) === false) {
            $this->readBy[] = $readBy;
        }

        return $this;
    }

    /**
     * Get readBy
     *
     * @return \array
     */
    public function getReadBy()
    {
        return $this->readBy;
    }

    /**
     * @param $user integer User id to check
     * @return array
     */
    public function hasReadBy($user)
    {
        return (array_search($user, $this->readBy) !== false);
    }

    /**
     * @return array
     */
    public function getReceivers()
    {
        return $this->receivers;
    }

    /**
     * @param $user integer User id to check
     * @return array
     */
    public function hasReceiver($user)
    {
        return (array_search($user, $this->receivers) !== false);
    }

    /**
     * @param mixed $receivers
     * @return Notification
     */
    public function setReceivers($receivers)
    {
        $this->receivers = $receivers;
        return $this;
    }

    /**
     * @return NotificationTemplate
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     * @return Notification
     */
    public function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @param null $index
     * @return array
     */
    public function getData($index = null)
    {
        if($index) {
            if(array_key_exists($index, $this->data)) {
                return $this->data[$index];
            }
            return null;
        }
        return $this->data;
    }

    /**
     * @param array $data
     * @return Notification
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @param array $data
     * @return Notification
     */
    public function addData($data)
    {
        $this->data = array_merge($this->data, $data);
        return $this;
    }


}
