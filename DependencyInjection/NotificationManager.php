<?php

namespace Trendix\NotificationsBundle\DependencyInjection;

use DateTime;
use Doctrine\ORM\EntityManager;
use Trendix\NotificationsBundle\Entity\Notification;
use Swift_Message;
use Symfony\Component\DependencyInjection\Container;
use Trendix\AdminBundle\Entity\User;

class NotificationManager
{
    public $em;
    public $container;

    public function __construct(Container $container, EntityManager $em) {
        $this->em = $em;
        $this->container = $container;
    }

    /** Creates a new notification and sends an email to the user
     * @param $users array of users who has to receive the notification
     * @param $link string Link the user will be sent to
     * @param $eventType string Type of Event
     * @param array $data Associative array with the data for the text strings
     * @param bool $sendEmail
     * @throws \Exception
     * @throws \Throwable
     */
    public function newNotification($users, $link, $eventType, $data = array(), $sendEmail = true)
    {
        $template = $this->em->getRepository('TrendixNotificationsBundle:NotificationTemplate')
            ->findOneBy(array('eventType' => $eventType));
        $notification = new Notification($link, $template, $users, $data);
        $this->em->persist($notification);
        $this->em->flush();
        $translator = $this->container->get('translator');
        $subject = $translator->trans($template->getEmailSubject(), $data);
        $data_array = array('link' => $link);
        $data_array = array_merge($data_array, $notification->getData());
        $body = $translator->trans($template->getEmailBody(), $data_array);
        $send = $this->container->getParameter('trendix_notifications.send_email');
        if($sendEmail && $send && $send != 'false') {
            $userClass = $this->container->getParameter('trendix_notifications.user_class');
            foreach($users as $id) {
                $user = $this->em->getRepository($userClass)->find($id);
                $message = Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom('info@bankify.es')
                    ->setTo($user->getEmail())
                    ->setBody($body, 'text/html');
                $this->container->get('mailer')->send($message);
            }
        }
    }

    /**
     * @param $user integer
     * @param null $lastDate
     * @param null $lastId
     * @param int $number If null or 0, returns all notifications
     * @param array $options Data field filter
     * @return mixed Last read and unread notifications for the user
     * @throws \Exception
     * @throws \Throwable
     */
    public function getLastNotifications($user, $lastDate = null, $lastId = null, $number = 5, $options = array())
    {
        $notifications = $this->em->getRepository('TrendixNotificationsBundle:Notification')
            ->findLastUserNotifications($user, $lastDate, $lastId, $number, $options);
        $translator = $this->container->get('translator');
        $result = array();
        for($i = 0; $i < count($notifications); $i++) {
            $data = unserialize($notifications[$i]['data']);
            $flag = true;
            foreach($options as $field => $value) {
                if(!isset($data[$field]) || $data[$field] != $value) {
                    $flag = false;
                }
            }
            if($flag) {
                $data_array = array('%link%' => $notifications[$i]['link']);
                $data_array = array_merge($data_array, $data);
                $notifications[$i]['subject'] = $translator->trans($notifications[$i]['subject'], $data_array);
                $notifications[$i]['body'] = $translator->trans($notifications[$i]['body'], $data_array);
                $notifications[$i]['data'] = json_encode(unserialize($notifications[$i]['data']));
                $result[] = $notifications[$i];
            }
        }
        return $result;
    }

    /** Stores the date a notifications has been read at.
     * @param $id integer ID of the notification.
     * @param $user integer ID of the reading user
     */
    public function readNotification($id, $user)
    {
        $notification = $this->em->getRepository('TrendixNotificationsBundle:Notification')
            ->find($id);
        $notification->addReadBy($user);
        $this->em->persist($notification);
        $this->em->flush();
    }


    /**
     * @param $user integer ID of the user whose notifications we want to read
     * @return integer Unread notifications number
     */
    public function getUnreadNotificationsNumber($user, $options = array())
    {
        if(count($options) > 0) {
            $notifications = $this->getLastNotifications($user, null, null, null, $options);
            $number = 0;
            for($i = 0; $i < count($notifications); $i++) {
                if(!$notifications[$i]['read']) {
                    $number++;
                }
            }
            return array('nb' => $number);
        } else {
            $number = $this->em->getRepository('TrendixNotificationsBundle:Notification')
                ->findUnreadNotificationsCount($user);
        }
        return $number;
    }
}