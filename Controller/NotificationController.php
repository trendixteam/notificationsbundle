<?php

namespace Trendix\NotificationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

class NotificationController extends Controller
{
    /**
     * @Route("read", name="notification_read")
     */
    public function readAction(Request $request)
    {
        $id = $request->get('id');
        $url = $request->get('url');
        $em = $this->get('doctrine')->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $notification = $em->getRepository('TrendixNotificationsBundle:Notification')
            ->find($id);
        if(!$notification->hasReceiver($user->getId())) {
            throw new AccessDeniedException();
        }
        $this->get('trendix_notifications')->readNotification($id, $user->getId());
        return $this->redirect($url);
    }

    /**
     * @Route("last/{number}", defaults={"number"=null}, name="notification_last")
     */
    public function lastNotificationsAction($number, Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $notifications = $this->get('trendix_notifications')->getLastNotifications($user, $number);
        return new JsonResponse($notifications);
    }

    /**
     * @Route("unread_number", name="notification_unread_number")
     */
    public function unreadNotificationsAction(Request $request)
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        $number = $this->get('trendix_notifications')->getUnreadNotificationsNumber($user);
        return new JsonResponse(array('count' => $number));
    }
}
