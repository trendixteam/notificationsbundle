<?php

namespace Trendix\NotificationsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('TrendixNotificationsBundle:Default:index.html.twig');
    }
}
